'use strict';

const CLIP_ROTATION = true;
const CLIP_POSITION = false;
const USE_COSINE = false;
const TRANSLATE_ORIGIN = true;

var vec2 = glMatrix.vec2;
var vec3 = glMatrix.vec3;
var vec4 = glMatrix.vec4;
var mat4 = glMatrix.mat4;
var quat = glMatrix.quat;
var quat2 = glMatrix.quat2;

/**
 * Attach trackball to element
 * @param {Element} target - The DOM element (normally a WebGL canvas) to attach the trackball to
 * @param {Function} hook - The callback function for trackball events: hook(rotation, translation, scale)
 */
function attachTrackball(target, hook) {

    // Rotation algorithm:
    //
    // 1. For each input event calculate NDC coordinates,
    //    i.e. normalize X and Y coordinates to range [-1, 1]
    //
    // 2. Synthesize the corresponding Z coordinate to lift the [X, Y]
    //    vector into three dimensions.  The formula for Z needs to be
    //    like this:
    //
    //       (1) We want the value of Z close to 1 in the middle of
    //           the element, gradually reducing to 0 as we approach
    //           the boundary.
    //
    //       (2) We don't want crazy high derivatives dZ/dX or dZ/dY
    //           anywhere.
    //
    //    Two example formulas that work reasonably well are
    //    Z=1-(X^2+Y^2) and Z=cos(sqrt(X^2+Y^2)*2*pi).
    //
    // 3. Take the [X, Y, Z] direction vectors from two consecutive
    //    events and calculate the 3D rotation between the vectors.
    //    Because of how we synthesized the direction vectors, the
    //    rotations resulting from drags near the middle of the
    //    element are mostly around X and Y axes, and the rotations
    //    near the boundary are around the Z axis.
    //
    // 4. Accumulate rotations incrementally as dragging continues.

    // Output state
    var rotation, translation, scale;

    // Bounding rectangle
    var bounds;

    // Tracking state
    var activeTouches, lastX, lastY, lastDX, lastDY, lastDist;

    // Hoisted locals
    var dir, lastDir, deltaRotation;

    // Initialize objects
    rotation = quat.create();
    translation = vec2.create();
    scale = 1.0;
    lastDir = vec3.create();
    dir = vec3.create();
    deltaRotation = quat.create();

    // Clip value to range
    function clip(x, min, max) {
        return Math.min(max, Math.max(min, x))
    }

    // Convert X coordinate to NDC
    function toNdcX(x) {
        x = ((x - bounds.left) * 2 - bounds.width) / bounds.width;
        if (CLIP_POSITION)
            x = clip(x, -1, 1);
        return x;
    }

    // Convert Y coordinate to NDC
    function toNdcY(y) {
        y = ((bounds.top - y) * 2 + bounds.height) / bounds.height;
        if (CLIP_POSITION)
            y = clip(y, -1, 1);
        return y;
    }

    // Translate X coordinate of rotation origin
    function translateX(x) {
        if (TRANSLATE_ORIGIN)
            x -= translation[0];
        return x;
    }

    // Translate Y coordinate of rotation origin
    function translateY(y) {
        if (TRANSLATE_ORIGIN)
            y -= translation[1];
        return y;
    }

    // Calculate direction vector
    function calcDir(out, x, y) {
        let r = Math.sqrt(x * x + y * y);
        if (CLIP_ROTATION) {
            if (r > 1)
                r = 1;
        }

        out[0] = x;
        out[1] = y;
        out[2] = USE_COSINE ? Math.cos(r * Math.PI / 2) : 1 - r * r;

        vec3.normalize(out, out);
        return out;
    }

    // Apply additional rotation
    function addRotation(x, y) {
        // Normalize coordinates
        x = toNdcX(x);
        y = toNdcY(y);

        // Calculate direction vectors
        calcDir(lastDir, translateX(lastX), translateY(lastY));
        calcDir(dir, translateX(x), translateY(y));

        // Rotate by delta rotation between direction vectors
        quat.rotationTo(deltaRotation, lastDir, dir);
        quat.multiply(rotation, deltaRotation, rotation);
        quat.multiply(rotation, deltaRotation, rotation);

        // Update stored last position
        lastX = x;
        lastY = y;
    }

    // Apply additional translation
    function addTranslation(x, y) {
        // Normalize coordinates
        x = toNdcX(x);
        y = toNdcY(y);

        // Calculate translation
        translation[0] += x - lastX;
        translation[1] += y - lastY;

        // Update stored last position
        lastX = x;
        lastY = y;
    }

    // Apply additional vertical rotation
    function addRotationZ(dx, dy) {
        // Set up direction vectors
        vec3.set(lastDir, lastDX, lastDY, 0); 
        vec3.set(dir, dx, dy, 0);

        // Normalize
        vec3.normalize(lastDir, lastDir);
        vec3.normalize(dir, dir);

        // Rotate by angle between the vectors
        quat.rotationTo(deltaRotation, dir, lastDir);
        quat.multiply(rotation, deltaRotation, rotation);

        // Update stores deltas
        lastDX = dx;
        lastDY = dy;
    }

    // Apply incremental scaling
    function addScaling(dist) {
        // Scale per distance between touch contacts
        scale *= dist / lastDist;

        // Update stored distance
        lastDist = dist;
    }

    // Select mouse rotation vs. pan based on which mouse button is
    // being pressed and which keyboard modifier keys are being held
    function isPanEvent(e) {
        return e.ctrlKey;
    }

    function startMouse(e) {
        e = e || window.event;
        e.preventDefault();
        e.stopPropagation();

        // Store bounding rectangle
        bounds = target.getBoundingClientRect();

        // Store initial position
        lastX = toNdcX(e.clientX);
        lastY = toNdcY(e.clientY);

        window.addEventListener("mousemove", updateMouse, false);
        window.addEventListener("mouseup", finishMouse, false);
        window.addEventListener("blur", finishMouse, false);
    }

    function updateMouse(e) {
        e = e || window.event;
        e.preventDefault();
        e.stopPropagation();

        // Update rotation/translation as appropriate
        if (isPanEvent(e))
            addTranslation(e.clientX, e.clientY);
        else
            addRotation(e.clientX, e.clientY);

        // Notify client
        hook(rotation, translation, scale);
    }

    function finishMouse(e) {
        e = e || window.event;
        e.preventDefault();
        e.stopPropagation();

        window.removeEventListener("mousemove", updateMouse);
        window.removeEventListener("mouseup", finishMouse);
        window.removeEventListener("blur", finishMouse);
    }

    // Copy touch list
    function copyTouches(tl) {
        let touches = [];
        for (let i = 0; i < tl.length; ++i) {
            touches.push({
                id: tl.item(i).identifier,
                x: tl.item(i).clientX,
                y: tl.item(i).clientY
            });
        }
        return touches;
    }

    // Reorder touch contacts by identifier
    function matchTouches(activeTouches, tl) {
        if (tl.length !== activeTouches.length)
            return null;
        let touches = copyTouches(tl);
        touches = activeTouches.map(x => touches.find(y => y.id === x.id));
        if (touches.find(x => x === undefined))
            return null;
        return touches;
    }

    function changeTouch(e) {
        e.preventDefault();
        e.stopPropagation();

        // Store bounding rectangle
        bounds = target.getBoundingClientRect();

        // Start touch processing per number of touch contacts
        switch (e.touches.length) {
        case 1:
            activeTouches = [];
            lastX = toNdcX(e.touches.item(0).clientX);
            lastY = toNdcY(e.touches.item(0).clientY);
            break;

        case 2:
            activeTouches = copyTouches(e.touches);
            lastX = toNdcX((activeTouches[0].x + activeTouches[1].x) / 2);
            lastY = toNdcY((activeTouches[0].y + activeTouches[1].y) / 2);
            lastDX = activeTouches[1].x - activeTouches[0].x;
            lastDY = activeTouches[1].y - activeTouches[0].y;
            lastDist = Math.sqrt(lastDX * lastDX + lastDY * lastDY)
            break;

        default:
            activeTouches = [];
            break;
        }
    }

    function updateTouch(e) {
        e.preventDefault();
        e.stopPropagation();

        // Dispatch on number of touch contacts
        switch (e.touches.length) {
        case 1:
            // Update rotation
            addRotation(e.touches.item(0).clientX, e.touches.item(0).clientY);

            // Send notification
            hook(rotation, translation, scale);
            break;

        case 2:
            let touches = matchTouches(activeTouches, e.touches);
            if (touches === null)
                break;

            let x = (touches[0].x + touches[1].x) / 2,
                y = (touches[0].y + touches[1].y) / 2,
                dx = touches[1].x - touches[0].x,
                dy = touches[1].y - touches[0].y,
                dist = Math.sqrt(dx * dx + dy * dy);

            // Pan, rotate around Z axis, and scale
            addTranslation(x, y);
            addRotationZ(dx, dy);
            addScaling(dist);

            // Send notification
            hook(rotation, translation, scale);
            break;
        }
    }

    function attachEvents() {
        target.addEventListener("mousedown", startMouse, false);
        target.addEventListener("touchstart", changeTouch, false);
        target.addEventListener("touchmove", updateTouch, false);
        target.addEventListener("touchend", changeTouch, false);
        target.addEventListener("touchcancel", changeTouch, false);
    }

    function detachEvents() {
        target.removeEventListener("mousedown", startMouse);
        window.removeEventListener("mousemove", updateMouse);
        window.removeEventListener("mouseup", finishMouse);
        window.removeEventListener("blur", finishMouse);
        target.removeEventListener("touchstart", changeTouch);
        target.removeEventListener("touchmove", updateTouch);
        target.removeEventListener("touchend", changeTouch);
        target.removeEventListener("touchcancel", changeTouch);
    }

    attachEvents();

    return {
        attach: attachEvents,

        detach: detachEvents,

        set: function set(newRotation = null, newTranslation = null, newScale = null) {
            if (newRotation)
                quat.copy(rotation, newRotation);
            if (newTranslation)
                vec2.copy(translation, newTranslation);
            if (newScale)
                scale = newScale;
        },
    };
}
