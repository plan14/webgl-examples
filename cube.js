'use strict';

var vec3 = glMatrix.vec3;
var vec4 = glMatrix.vec4;
var mat4 = glMatrix.mat4;
var quat = glMatrix.quat;

// The triangle
var triangleVertices = [
    //   X,   Y,    R,   G,   B
     0.0,  0.5,  1.0, 0.0, 0.0,
    -0.5, -0.5,  0.0, 1.0, 0.0,
     0.5, -0.5,  0.0, 0.0, 1.0,
    -0.9,  0.9,  0.5, 0.0, 0.0,
    -0.8,  0.6,  0.2, 0.5, 0.0,
    -0.6,  0.9,  0.4, 0.0, 0.5,
];

let triangleIndices = [
    0, 1, 2,
    3, 4, 5,
];

// The cube
let cubeVertices = [
    // xyz              rgb              tex     norm
    // Top
    -1.0, -1.0, 1.05,   1.0, 1.0, 0.5,   0, 0,   0.0, 0.0, 1.0,
    -1.0,  1.0, 1.05,   1.0, 1.0, 1.0,   0, 1,   0.0, 0.0, 1.0,
     1.0,  1.0, 1.05,   1.0, 1.0, 1.0,   1, 1,   0.0, 0.0, 1.0,
     1.0, -1.0, 1.05,   1.0, 1.0, 1.0,   1, 0,   0.0, 0.0, 1.0,

    // Bottom
     1.0, -1.0, -1.05,  1.0, 0.84, 0,   1, 0,   0.0, 0.0, -1.0,
     1.0,  1.0, -1.05,  1.0, 0.84, 0,   1, 1,   0.0, 0.0, -1.0,
    -1.0,  1.0, -1.05,  1.0, 0.84, 0,   0, 1,   0.0, 0.0, -1.0,
    -1.0, -1.0, -1.05,  1.0, 0.84, 0,   0, 0,   0.0, 0.0, -1.0,

    // Left
    -1.05, -1.0, -1.0,  1.0, 0.35, 0.0,   0, 0,   -1.0, 0.0, 0.0,
    -1.05,  1.0, -1.0,  1.0, 0.35, 0.0,   1, 0,   -1.0, 0.0, 0.0,
    -1.05,  1.0,  1.0,  1.0, 0.35, 0.0,   1, 1,   -1.0, 0.0, 0.0,
    -1.05, -1.0,  1.0,  1.0, 0.35, 0.0,   0, 1,   -1.0, 0.0, 0.0,

    // Right
     1.05, -1.0,  1.0,  0.73, 0.0, 0.0,   0, 1,   1.0, 0.0, 0.0,
     1.05,  1.0,  1.0,  0.73, 0.0, 0.0,   1, 1,   1.0, 0.0, 0.0,
     1.05,  1.0, -1.0,  0.73, 0.0, 0.0,   1, 0,   1.0, 0.0, 0.0,
     1.05, -1.0, -1.0,  0.73, 0.0, 0.0,   0, 0,   1.0, 0.0, 0.0,

    // Front
    -1.0, -1.05, -1.0,  0.0, 0.61, 0.28,   0, 0,   0.0, -1.0, 0.0,
    -1.0, -1.05,  1.0,  0.0, 0.61, 0.28,   0, 1,   0.0, -1.0, 0.0,
     1.0, -1.05,  1.0,  0.0, 0.61, 0.28,   1, 1,   0.0, -1.0, 0.0,
     1.0, -1.05, -1.0,  0.0, 0.61, 0.28,   1, 0,   0.0, -1.0, 0.0,

    // Back
     1.0,  1.05, -1.0,  0.0, 0.27, 0.68,   1, 0,   0.0, 1.0, 0.0,
     1.0,  1.05,  1.0,  0.0, 0.27, 0.68,   1, 1,   0.0, 1.0, 0.0,
    -1.0,  1.05,  1.0,  0.0, 0.27, 0.68,   0, 1,   0.0, 1.0, 0.0,
    -1.0,  1.05, -1.0,  0.0, 0.27, 0.68,   0, 0,   0.0, 1.0, 0.0,
];

let cubeIndices = [
     0,  3,  2,
     0,  2,  1,
     4,  7,  6,
     4,  6,  5,
     8, 11, 10,
     8, 10,  9,
    12, 15, 14,
    12, 14, 13,
    16, 19, 18,
    16, 18, 17,
    20, 23, 22,
    20, 22, 21,

    3, 0, 17,
    3, 17, 18,
    1, 2, 21,
    1, 21, 22,
    5, 6, 23,
    23, 20, 5,
    4, 16, 7,
    16, 4, 19,
    0, 1, 11,
    1, 10, 11,
    17, 8, 16,
    17, 11, 8,
    0, 11, 17,
    9, 10, 22,
    22, 23, 9,
    8, 9, 6,
    8, 6, 7,
    1, 22, 10,
    7, 16, 8,
    6, 9, 23,
    2, 13, 21,
    3, 13, 2,
    12, 13, 3,
    15, 18, 19,
    18, 15, 12,
    14, 15, 4,
    4, 5, 14,
    20, 21, 13,
    13, 14, 20,
    3, 18, 12,
    5, 20, 14,
    4, 15, 19,
];

// Get WebGL context
function initGl(selector)
{
    const canvas = document.querySelector(selector);
    if (canvas == null) {
        alert('Unable to get selector' + selector);
        return null;
    }

    // Initialize the GL context
    const gl = canvas.getContext('webgl');
    if (!gl) {
        alert('WebGL not supported, falling back on experimental');
        gl = canvas.getContext('experimental-webgl');
        if (!gl) {
            alert('Your browser or your computer does not support WebGL');
        }
    }

    return gl;
}

var resources = {
    vertexShader: "shader.vs.glsl",
    fragmentShader: "shader.fs.glsl",
};

function loadTextResource(url, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            if (request.status >= 200 && request.status < 300)
                callback(request.status, request.responseText);
            else
                callback(request.status, null);
        }
    }
    request.open("GET", url, true);
    request.send();
}

function loadResources(resources, callback) {
    let keys = Object.keys(resources);
    function loop(i, keys) {
        if (i < keys.length) {
            let url = resources[keys[i]];
            if (typeof url === "object")
                url = url.url;
            function hook(status, text) {
                resources[keys[i]] = {
                    url: url,
                    status: status,
                    text: text,
                };
                loop(i + 1, keys);
            }
            loadTextResource(url, hook);
        }
        else {
            let success = keys.every(x => resources[x].text !== null);
            callback(success);
        }
    }
    loop(0, keys);
}

function main() {
    loadResources(resources, main1);
}

// Start here
var reset = function () { };
function main1() {
    //alert('No way this is working!');

    const gl = initGl("#gl-canvas");

    // Set clear color to black, fully opaque
    gl.clearColor(0.25, 0.35, 0.3, 1.0);

    // Clear the color buffer with specified clear color
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Create vertex program
    let vertexShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vertexShader, resources.vertexShader.text);
    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader', gl.getShaderInfoLog(vertexShader));
        return;
    }

    // Create fragment shader
    let fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fragmentShader, resources.fragmentShader.text);
    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling fragment shader', gl.getShaderInfoLog(fragmentShader));
        return;
    }

    // Create GL program
    let program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        console.error('ERROR linking program', gl.getProgramInfoLog(program));
        return;
    }

    // Validate GL program
    gl.validateProgram(program);
    if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
        console.error('ERROR validating program', gl.getProgramInfoLog(program));
        return;
    }

    // Vertex buffer and attributes
    let cubeVertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(cubeVertices), gl.STATIC_DRAW);

    let positionAttribLoc = gl.getAttribLocation(program, 'vertPosition');
    gl.vertexAttribPointer(
        positionAttribLoc,  // Attribute location
        3,                  // Number of elements per each
        gl.FLOAT,           // Type of elements
        gl.FALSE,           // Normalized or not
        11 * Float32Array.BYTES_PER_ELEMENT,  // Size of each vertex
        0                  // Offset from the beginning of a vertex to this attribute
    );
    gl.enableVertexAttribArray(positionAttribLoc);

    let colorAttribLoc = gl.getAttribLocation(program, 'vertColor');
    gl.vertexAttribPointer(
        colorAttribLoc,  // Attribute location
        3,               // Number of elements per each
        gl.FLOAT,        // Type of elements
        gl.FALSE,        // Normalized or not
        11 * Float32Array.BYTES_PER_ELEMENT,  // Size of each vertex
        3 * Float32Array.BYTES_PER_ELEMENT    // Offset from the beginning of a vertex to this attribute
    );
    gl.enableVertexAttribArray(colorAttribLoc);

    let texCoordAttribLoc = gl.getAttribLocation(program, 'vertTexCoord');
    gl.vertexAttribPointer(
        texCoordAttribLoc,  // Attribute location
        2,                  // Number of elements per each
        gl.FLOAT,           // Type of elements
        gl.FALSE,           // Normalized or not
        11 * Float32Array.BYTES_PER_ELEMENT,  // Size of each vertex
        6 * Float32Array.BYTES_PER_ELEMENT    // Offset from the beginning of a vertex to this attribute
    );
    gl.enableVertexAttribArray(texCoordAttribLoc);

    let vertNormalAttribLoc = gl.getAttribLocation(program, 'vertNormal');
    gl.vertexAttribPointer(
        vertNormalAttribLoc, // Attribute location
        3,                   // Number of elements per each
        gl.FLOAT,            // Type of elements
        gl.TRUE,             // Normalized or not
        11 * Float32Array.BYTES_PER_ELEMENT,  // Size of each vertex
        8 * Float32Array.BYTES_PER_ELEMENT    // Offset from the beginning of a vertex to this attribute
    );
    gl.enableVertexAttribArray(vertNormalAttribLoc);
    //gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // Index buffer
    let cubeIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeIndexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeIndices), gl.STATIC_DRAW);
    //gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

    // Texture
    var cubeTexture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, document.getElementById("texture1"));
    //gl.bindTexture(gl.TEXTURE_2D, null);

    gl.useProgram(program);
    gl.frontFace(gl.CCW);
    gl.cullFace(gl.BACK);
    gl.enable(gl.CULL_FACE);

    let mTransform = mat4.create();
    let mTransformLoc = gl.getUniformLocation(program, 'mTransform');

    // Main render loop
    let rotQuat = quat.create();
    let transVec = vec3.create();
    let mainScale = 1.0;
    let resetMe = 0;
    let resetFrame = 0;
    let animQuat = quat.create();
    let nullQuat = quat.create();
    let animVec = vec3.create();
    let nullVec = vec3.create();
    let animScale = 1.0;
    main.angle = 0;
    let loop = function () {
        let scale = [0.3, 0.3, 0.3];
        vec3.scale(scale, scale, mainScale);
        mat4.fromRotationTranslationScale(mTransform, rotQuat, [transVec[0], transVec[1], 0.0], scale);

        gl.uniformMatrix4fv(mTransformLoc, gl.FALSE, mTransform);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.drawElements(gl.TRIANGLES, cubeIndices.length, gl.UNSIGNED_SHORT, 0);

        if (resetMe === 1) {
            ctlo.detach();
            animQuat = rotQuat;
            animVec = transVec;
            animScale = mainScale;
            resetMe = 2;
            resetFrame = 0;
            requestAnimationFrame(loop);
        }
        else if (resetMe === 2) {
            let t = resetFrame / 100;
            quat.slerp(rotQuat, animQuat, nullQuat, t);
            vec3.lerp(transVec, animVec, nullVec, t);
            mainScale = animScale ** (1 - t);
            ctlo.set(rotQuat, transVec, mainScale);
            document.getElementById("diag-text").textContent = t;
            if (resetFrame++ == 100) {
                resetMe = 0;
                ctlo.attach();
            }
            else {
                requestAnimationFrame(loop);
            }
        }
    };
    requestAnimationFrame(loop);

    // Reset
    reset = function () {
        resetMe = 1;
        requestAnimationFrame(loop);
    }

    // Mouse motion
    let canvas = document.querySelector("#gl-canvas");
    var ctlo = attachTrackball(canvas, function (rot, trans, scale) {
        rotQuat = rot;
        transVec = trans;
        mainScale = scale;
        requestAnimationFrame(loop);
    });
}

function wheelStep() {
    // Note: static variable wheelStep.step starts off as undefined,
    // initialized on first call because gcd(undefined,N)=N
    wheelStep.step = gcd(wheelStep.step, event.deltaY);

    main.angle += event.deltaY / wheelStep.step * Math.PI / 180;

    console.log(event.deltaY / wheelStep.step + '*' + wheelStep.step + ', ' + main.angle);
}

function gcd(a, b) {
    a = Math.abs(a);
    b = Math.abs(b);
    while (b) {
        let r = a % b;
        a = b;
        b = r;
    }
    return a;
}
